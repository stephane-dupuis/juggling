// Import du package http
var http = require("http")
// Import du package BufferList
var bl = require("bl")

// Tableau des résultat
var tab = []
// Compteur permettant de savoir s'il faut faire l'impression
var compteur=0

// Fonction pour catcher les requêtes HTTP
function go(indice){
  http.get(process.argv[indice+2], function (response){
    response.pipe(bl(function (err, data) {
      if (err) {
          console.log("err")
        }

        // Ecriture du paramètre
    		tab[compteur]=data.toString()

        compteur++

        // Sommes-nous en fin de cycle
        if (compteur == 3) {
          // On parse le tableau pour l'écrire dans la console
          for (var i=0;i<3;i++){
            console.log(tab[i])
          }
        }        
    }))
  })
}

// Boucle pour catcher les requêtes HTTP
for (var i=0;i<3;i++){
  go(i)
}





